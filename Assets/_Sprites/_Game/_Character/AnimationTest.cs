﻿using UnityEngine;
using System.Collections;

public class AnimationTest : MonoBehaviour 
{

	Animator _Animator;

	public enum CurrentAnimation { 
		idle, squat, jump, land 
	}
	
	public CurrentAnimation currentAnimation;

	public AnimatorClipInfo[] info;

	void Start () 
	{
		this._Animator = this.gameObject.GetComponent <Animator> ();
	}

	public bool isPlaying;
	
	void Update () 
	{
		switch (currentAnimation)
		{
		case CurrentAnimation.idle:
			_Animator.Play ("Breathe");
			break;
		case CurrentAnimation.squat:
			_Animator.Play ("Squat");
			break;
		case CurrentAnimation.jump:
			_Animator.Play ("Jump");
			break;
		case CurrentAnimation.land:
			_Animator.Play ("Landing");
			break;
		default:
			_Animator.Play ("Breathe");
			break;
		}

		//AnimationInfo[] ainfo = some.GetCurrentAnimationClipState(0);
		info = _Animator.GetCurrentAnimatorClipInfo (0);

		if (Input.GetMouseButtonDown (0))
		{
			this.currentAnimation = CurrentAnimation.squat;
		}
		else if (Input.GetMouseButtonUp (0))
		{
			this.currentAnimation = CurrentAnimation.jump;
		}

		if (Input.GetKeyDown (KeyCode.Space))
		{
			this.currentAnimation = CurrentAnimation.land;
		}

		if (_Animator.GetCurrentAnimatorStateInfo (0).normalizedTime >= 0.99f && !Input.GetMouseButton (0))
		{
			//this.currentAnimation = CurrentAnimation.idle;
		}

		this.isPlaying = (_Animator.GetCurrentAnimatorStateInfo (0).normalizedTime <= 0.99f);

		//isPlaying = inf.normalizedTime >= 0.99f
	}
}
