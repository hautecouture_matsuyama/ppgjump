﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFireballTag : MonoBehaviour {

    private void Awake()
    {
        this.gameObject.tag = "Fireball";
    }
}
